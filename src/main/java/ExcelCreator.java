import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

public class ExcelCreator {

    public boolean generateExcel(String sourcePath, String resultPath) {

        List<String> result = new ArrayList<>();
        File directoryPath = new File(sourcePath);
        String[] contents = directoryPath.list();
        List<String> sFileList = Arrays.asList(contents);


        try {
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet sheet = workbook.createSheet("sheet1");

            for (int i=0 ; i < sFileList.size() ; i++) {
                HSSFRow row = sheet.createRow((short) i);
                row.createCell(0).setCellValue(sFileList.get(i));
            }

            String filename = resultPath + "/Balance.xls";
            FileOutputStream fileOut = new FileOutputStream(filename);
            workbook.write(fileOut);
            fileOut.close();
            workbook.close();
            System.out.println("Excel file has been generated successfully.");

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }
}
